<?php

//esimene asi mille kutsume, et info ära ei kaoks
session_start();

// loome csrf tokeni, turvalisuse huvides
if (empty($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(20));
}

// vajalikud meetodid
require 'model.php';
require 'controller.php';

// rakenduse "ruuter"
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    // $result muutuja indikeerib kas toimus mõni õnnestunud tegevus või mitte
    // praegu pole enam vajalik
    $result = false;
    
    // Lubame postitustegevused ainult juhul kui päringuga tuleb kaasa korrektne CSRF token.
    // Eeldab, et paneksime kõikidesse lehe vormidesse selle tokeni peidetud väljana sisse
    if (!empty($_POST['csrf_token']) && $_POST['csrf_token'] == $_SESSION['csrf_token']) {
        switch ($_POST['action']) {
            
            case 'add':
                $nimetus = $_POST['nimetus'];
                $kogus   = intval($_POST['kogus']);
                $result  = controller_add($nimetus, $kogus);
                break;
            
            case 'delete':
                $id     = intval($_POST['id']);
                $result = controller_delete($id);
                break;
            
            case 'update':
                $id     = intval($_POST['id']);
                $kogus  = intval($_POST['kogus']);
                $result = controller_update($id, $kogus);
                break;
            
            case 'register':
                $kasutajanimi = $_POST['kasutajanimi'];
                $parool       = $_POST['parool'];
                $parool2      = $_POST['parool2'];
                $result       = controller_register($kasutajanimi, $parool, $parool2);
                break;
            
            case 'login':
                $kasutajanimi = $_POST['kasutajanimi'];
                $parool       = $_POST['parool'];
                $result       = controller_login($kasutajanimi, $parool);
                break;
            
            case 'logout':
                $result = controller_logout();
                break;
            case 'del_account':
                
                $result = controller_del_account($kasutajanimi, $parool, $id);
                break;
        }
    }
    
    if ($result) {
        // kuna $result on true siis järelikult toimus mõni õnnestunud tegevus
        // sellisel juhul suuname kasutaja tagasi eelmisele lehele
        header('Location: ' . $_SERVER['PHP_SELF']);
    } else {
        header('Content-type: text/plain; charset=utf-8');
        echo 'Päring ebaõnnestus!';
    }
    
    // POST päringu puhul me sisu ei näita
    exit;
}

if (!empty($_GET['view'])) {
    switch ($_GET['view']) {
        case 'login':
            require 'view_login.php';
            break;
        case 'register':
            require 'view_register.php';
            break;
        default:
            header('Content-type: text/plain; charset=utf-8');
            echo 'Tundmatu valik!';
            exit;
    }
} else {
    if (!controller_user()) {
        header('Location: ' . $_SERVER['PHP_SELF'] . '?view=login');
        exit;
    }
    //vaatame kas page get muutuja on empty, siis page = 1
    if (empty($_GET['page'])) {
        $page = 1;
    } else {
        $page = intval($_GET['page']);
        if ($page <= 0) {
            $page = 1;
        }
    }
    
    require 'view.php';
}

mysqli_close($l);