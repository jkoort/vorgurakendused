<?php

function controller_add($nimetus, $kogus)
{
    if (!controller_user()) {
        message_add('Pead olema sisselogitud');
        return false;
    }
    
    //sisendite kontroll
    if ($nimetus == '' || $kogus <= 0) {
        message_add('Vigased andmed');
        return false;
    }
    
    if (model_add($nimetus, $kogus)) {
        message_add('Lisati uus rida');
        return true;
    }
    message_add('Andmete lisamine ebaõnnestus');
    return false;
}
//funktsioon rea kustutamiseks
function controller_delete($id)
{
    if (!controller_user()) {
        return false;
    }
    
    if ($id <= 0) {
        return false;
    }
    
    
    
    if (model_delete($id)) {
        message_add('Kustutati rida: ' . $id);
        return true;
    }
    return false;
    
}
//funktsioon rea uuendamiseks
function controller_update($id, $kogus)
{
    //kontrollib kasutaja sisselogitust
    if (!controller_user()) {
        return false;
    }
    
    if ($id <= 0 || $kogus <= 0) {
        return false;
    }
    
    if (model_update($id, $kogus)) {
        message_add('Muudeti rida: ' . $id);
        return true;
    }
    return false;
}

//funktsioon mis kontrollib kasutaja sisselogituse väärtust
function controller_user()
{
    if (empty($_SESSION['login'])) {
        return false;
    }
    
    return $_SESSION['login'];
}
//funktsioon registreerimaks kasutaja
function controller_register($kasutajanimi, $parool, $parool2)
{
    //parooli ja kasutajanime väli ei tohi olla tühjad
    if ($kasutajanimi == '' || $parool == '') {
        echo "Fields cannot be empty!";
        return false;
    }
    //paroolid peavad kattuma
    if ($parool != $parool2) {
        echo "Passwords must be same!";
        exit;
        return false;
        
    }
    //kasutajanimi ja parool peavad olema 8 ja 16 tähemärgi vahemikus
    if ((strlen($kasutajanimi) < 8) || (strlen($kasutajanimi) > 16) || (strlen($parool) < 8) || (strlen($parool) > 16)) {
        echo "Username and password must both be between 8 to 16 symbols long";
        
        exit;
    }
    
    return model_user_add($kasutajanimi, $parool);
}

function controller_del_account($kasutajanimi, $parool)
{
    return model_user_delete($kasutajanimi, $parool);
}
//funktsioon logimaks kasutaja sisse
function controller_login($kasutajanimi, $parool)
{
    
    if ($kasutajanimi == '' || $parool == '') {
		echo "Fields cannot be empty!";
        return false;
    }
	  if ((strlen($kasutajanimi) < 8) || (strlen($kasutajanimi) > 16) || (strlen($parool) < 8) || (strlen($parool) > 16)) {
        echo "Username and password must both be between 8 to 16 symbols long";
        exit;
    }
    
    $id = model_user_get($kasutajanimi, $parool);
    if (!$id) {
        return false;
    }
    
    session_regenerate_id();
    $_SESSION['login'] = $id;
    message_add('Welcome back ' . $kasutajanimi);
    return $id;
}
//funktsioon logimaks kasutaja välja
function controller_logout()
{
    
    if (!controller_user()) {
        return false;
    }
    
    // muudab sessiooni küpsise kehtetuks
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time() - 42000, '/');
    }
    
    // tühjenda sessiooni massiiv
    $_SESSION = array();
    // lõpeta sessioon
    
    session_destroy();
    
    
    return true;
}
//funktsioon, mis lisab sõnumid massiivi
function message_add($message)
{
    if (empty($_SESSION['messages'])) {
        $_SESSION['messages'] = array();
    }
    $_SESSION['messages'][] = $message;
}

function message_list()
{
    if (empty($_SESSION['messages'])) {
        return array();
    }
    $messages             = $_SESSION['messages'];
    $_SESSION['messages'] = array();
    return $messages;
}