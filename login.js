document.querySelector('#login1').addEventListener('click',
    /**
     * Funktsioon kontrollib v��rtusi
     * @event
     */
    function() {
        var kasutaja = document.getElementById('kasutajanimi').value;
        var parool = document.getElementById('parool').value;

        if (parool == '' || kasutaja == '') {
            event.preventDefault();
            alert('Fields cannot be empty!');

            document.getElementById("kasutajanimi").value = "";
            document.getElementById("parool").value = "";
            return;

        }


        if (kasutaja.length < 8 || kasutaja.length > 16) {
            event.preventDefault();
            alert('Vigane kasutajanimi!');

            document.getElementById("Vigane kasutajanimi").value = "";
            return false;
        }
        if (parool.length < 8 || parool.length > 16) {
            event.preventDefault();
            alert('Vigane parool');

            document.getElementById("parool").value = "";
            return;

        }
    });