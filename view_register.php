<!doctype html>
<html>
    <head>
        <meta charset="utf8" />
        <title>Registreeri konto</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="vorm.css"
    </head>
    <body>

        <h1>Registreeri konto</h1>

        <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">

            <input type="hidden" name="action" value="register">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
	
            <table>
                <tr>
                    <td>Kasutajanimi</td>
                    <td>
						<div class="input-group">
                        <input type="text" id="kasutajanimi" name="kasutajanimi"  placeholder="Kasutajanimi" required>
						</div>
                    </td>
                </tr>
                <tr>
                    <td>Parool</td>
                    <td>
					<div class="input-group">
                        <input type="password" id="parool" name="parool"  placeholder="Parool" required>
						</div>
                    </td>
                </tr>
				             <tr>
                    <td>Korda parooli</td>
                    <td>
					<div class="input-group">
                        <input type="password" id="parool2" name="parool2"  placeholder="Korda parooli" required>
						</div>
                    </td>
                </tr>
            </table>

             <p>
                <button id="register" type="submit" class="btn btn-success">Registreeri konto</button> või
				<a href="<?= $_SERVER['PHP_SELF']; ?>?view=login">Logi sisse</a>
            </p>

        </form>
			<script src="register.js"></script>
    </body>
</html>