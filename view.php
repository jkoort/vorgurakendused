<!doctype HTML>
<html>

<head>

    <title>Laoprogramm</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        #lisa-vorm {
            display: none;
        }
    </style>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css"


 
</head>

<body>

<?php
foreach (message_list() as $message):
?>
<p >
<div class="alert alert-info" role="alert">
	<?= $message; ?>
</div>
</p>
<?php
endforeach;
?>
    <div style="float: right;">
        <form method="post"  action="<?= $_SERVER['PHP_SELF']; ?>">
            <input type="hidden" name="action" value="logout">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
            <button type="submit" class="btn btn-success">Logi välja</button>
        </form>
    </div>
	    <div style="float: right;">
        <form method="post"  action="<?= $_SERVER['PHP_SELF']; ?>">
            <input type="hidden" name="action" value="del_account">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
		
            
			
        </form>
    </div>
	


    <h1>Laoprogramm</h1>

    <p id="kuva-nupp">
        <button type="button" class="btn btn-warning">Kuva lisamise vorm</button>
    </p>

    <form id="lisa-vorm" method="post" action="<?= $_SERVER['PHP_SELF']; ?>">

        <input type="hidden" name="action" value="add">
        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">

        <p id="peida-nupp">
            <button type="button" class="btn btn-warning">Peida lisamise vorm</button>
        </p>

        <table>
            <tr>
                <td>Nimetus</td>
                <td>
                    <input type="text" id="nimetus" name="nimetus">
                </td>
            </tr>
            <tr>
                <td>Kogus</td>
                <td>
                    <input type="number" id="kogus" name="kogus">
                </td>
            </tr>
        </table>

        <p>
            <button type="submit" class="btn btn-danger">Lisa kirje</button>
        </p>

    </form>

    <table id="ladu" border="1">
        <thead>
            <tr>
                <th>Nimetus</th>
                <th>Kogus</th>
                <th>Tegevused</th>
            </tr>
        </thead>

        <tbody>

        <?php
// koolon tsükli lõpus tähendab, et tsükkel koosneb HTML osast
foreach (model_load($page) as $rida):
?>

            <tr>
                <td>
                    <?= 
    // vältimaks pahatahtlikku XSS sisu, kus kasutaja sisestab õige
        
    // info asemel <script> tag'i, peame tekstiväljundis asendama kõik HTML erisümbolid
        htmlspecialchars($rida['nimetus']); ?>
                </td>
                <td>
                    <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                        <input type="hidden" name="action" value="update">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                        <input type="hidden" name="id" value="<?= $rida['id']; ?>">

                        <input type="number" style="width: 5em; text-align: right;" name="kogus" value="<?= $rida['kogus']; ?>">
                        <button type="submit" class="btn btn-info ">Uuenda</button>
                    </form>
                </td>
                <td>

                    <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                        <input type="hidden" name="action" value="delete">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                        <input type="hidden" name="id" value="<?= $rida['id']; ?>">
						<button type="submit"  class="btn btn-danger btn-sm">Kustuta</button>
                    </form>

                </td>
				
				
            </tr>

        <?php
endforeach;
?>

        </tbody>
    </table>

	<p>
	<nav>
	  <a class="page-link" href="#" aria-label="Previous">
		<a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $page - 1 ?>" >
		Previous page

		</a>
		<a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $page + 1 ?>">
		Next page
		</a>
	</nav>
	</p>

	
    <script src="ladu.js"></script>
</body>

</html>