<!doctype html>
<html>
    <head>
        <meta charset="utf8" />
        <title>Logi sisse</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="vorm.css"
    </head>
    <body>

        <h1>Logi sisse</h1>

        <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">

            <input type="hidden" name="action" value="login">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">


            <table>
                <tr>
                    <td>Kasutajanimi</td>
                    <td>
                        <input type="text"  id="kasutajanimi" name="kasutajanimi" placeholder="Kasutajanimi" required>
                    </td>
                </tr>
                <tr>
                    <td>Parool</td>
                    <td>
                        <input type="password" id="parool" name="parool"  placeholder="Parool" required>
                    </td>
                </tr>
            </table>

            <p>
				
                <button id="login1" type="submit"  class="btn btn-success" >Logi sisse</button>
				või
				<a href="<?= $_SERVER['PHP_SELF']; ?>?view=register">registreeri konto</a>
            </p>

        </form>
		 <script src="login.js"></script>
    </body>
</html>