document.querySelector('#register').addEventListener('click',
    /**
     * Funktsioon kontrollib sisendeid
     * @event
     */
    function(event) {
        var kasutaja = document.getElementById('kasutajanimi').value;
        var parool = document.getElementById('parool').value;
        var parool2 = document.getElementById('parool2').value;

        if (parool == '' || kasutaja == '') {
            event.preventDefault();
            alert('Fields cannot be empty!');

            document.getElementById("kasutajanimi").value = "";
            document.getElementById("parool").value = "";
            return;

        }
        if (parool2 != parool) {
            event.preventDefault();
            alert('Passwords must be same!');

            document.getElementById("parool").value = "";
            document.getElementById("parool2").value = "";
            return;
        }

        if (kasutaja.length < 8 || kasutaja.length > 16) {
            event.preventDefault();
            alert('Username length should be between 8 to 16 symbols!');

            document.getElementById("kasutajanimi").value = "";
            return;
        }
        if (parool.length < 8 || parool.length > 16) {
            event.preventDefault();
            alert('Password length should be between 8 to 16 symbols!');

            document.getElementById("parool").value = "";
            return;
        }
    });